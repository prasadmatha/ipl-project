//3. Extra runs conceded per team in the year 2016

const fs = require('fs')
const { matchesJson, deliveriesJson } = require("./index.js")

function getExtraRunsConcededPerTeamin2016(matchesJson, deliveriesJson) {
    let matchIdPlayedIn2016 = matchesJson.reduce((acc, curr) => {
        if (curr.season === "2016") {
            acc.push(curr.id)
        }
        return acc
    }, [])

    let extraRunsConcededPerTeam = deliveriesJson.reduce((acc, curr) => {
        if (matchIdPlayedIn2016.includes(curr.match_id)) {
            if (acc[curr.bowling_team]) {
                acc[curr.bowling_team] += parseInt(curr.extra_runs)
            }
            else {
                acc[curr.bowling_team] = parseInt(curr.extra_runs)
            }
        }
        return acc
    }, {})
    return extraRunsConcededPerTeam
}


const result = getExtraRunsConcededPerTeamin2016(matchesJson, deliveriesJson)
console.log(result)

const extraRunsConcededPerTeamIn2016JSON = JSON.stringify(result)

fs.writeFile("../public/output/extraRunsConcededPerTeam.json", extraRunsConcededPerTeamIn2016JSON, (err) => {
  if (err) {
    console.error(err)
    return
  }
  //file written successfully
})