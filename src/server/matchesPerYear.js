//1. Number of matches played per year for all the years in IPL.

const fs = require('fs')
const { matchesJson, deliveriesJson } = require("./index.js")

function getMatchesPerYear(matchesJson) {
    let matchesPlayedInEachYear = matchesJson.reduce((acc, curr) => {
        if (acc[curr.season]) {
            acc[curr.season] += 1
        }
        else {
            acc[curr.season] = 1
        }
        return acc
    }, {})
    return matchesPlayedInEachYear
}

const result = getMatchesPerYear(matchesJson)
console.log(result)

const matchesPerYearJSON = JSON.stringify(result)

fs.writeFile("../public/output/matchesPerYear.json", matchesPerYearJSON, (err) => {
  if (err) {
    console.error(err)
    return
  }
  //file written successfully
})