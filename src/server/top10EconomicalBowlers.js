//4. Top 10 economical bowlers in the year 2015

const fs = require('fs')
const { matchesJson, deliveriesJson } = require("./index.js")


function getTop10EconomicalBowlers(matchesJson, deliveriesJson) {
    let matchesIdIn2015 = matchesJson.filter(eachMatch => {
        return eachMatch.season === "2015"
    }).map(eachMatch => eachMatch.id)

    totalRunsConcededByABowler = deliveriesJson.reduce((acc, curr) => {
        if (matchesIdIn2015.includes(curr.match_id)) {
            acc[curr.bowler] ? acc[curr.bowler] += parseInt(curr.total_runs) : acc[curr.bowler] = parseInt(curr.total_runs)
        }
        return acc
    }, {})

    totalBallsBowledByABowler = deliveriesJson.reduce((acc, curr) => {
        if (matchesIdIn2015.includes(curr.match_id)) {
            if (acc[curr.bowler]) {
                (curr.wide_runs == 0 && curr.noball_runs == 0) ? acc[curr.bowler] += 1 : acc[curr.bowler] += 0
            }
            else {
                (curr.wide_runs == 0 && curr.noball_runs == 0) ? acc[curr.bowler] = 1 : acc[curr.bowler] = 0
            }
        }
        return acc
    }, {})

    let economyOfBowlers = Object.entries(deliveriesJson.reduce((acc, curr) => {
        if (!acc[curr.bowler] && matchesIdIn2015.includes(curr.match_id)) {
            acc[curr.bowler] = Number((totalRunsConcededByABowler[curr.bowler] / (totalBallsBowledByABowler[curr.bowler] / 6)).toFixed(2))
        }
        return acc
    }, {})).sort((firstElement, secondElement) => {
        if (firstElement[1] < secondElement[1]) return -1
        else if (firstElement[1] > secondElement[1]) return 1
        else return 0
    })
    return Object.fromEntries(economyOfBowlers.slice(0, 10))
}

const result = getTop10EconomicalBowlers(matchesJson, deliveriesJson)
console.log(result)

const top10EconomicalBowlersJSON = JSON.stringify(result)

fs.writeFile("../public/output/top10EconomicalBowlers.json", top10EconomicalBowlersJSON, (err) => {
  if (err) {
    console.error(err)
    return
  }
  //file written successfully
})