//Find the bowler with the best economy in super overs

const fs = require('fs')
const { matchesJson, deliveriesJson } = require("./index.js")

function getEconomyOfBowlers(deliveriesJson) {
    let ballsBowledInSuperOvers = deliveriesJson.reduce((acc, curr) => {
        if (curr.is_super_over == 1) {
            if (curr.wide_runs == 0 && curr.noball_runs == 0) {
                if (acc[curr.bowler]) {
                    acc[curr.bowler] += 1
                }
                else {
                    acc[curr.bowler] = 1
                }
            }
        }
        return acc
    }, {})

    let runsConcededInSuperOvers = deliveriesJson.reduce((acc, curr) => {
        if (curr.is_super_over == 1) {
            if (acc[curr.bowler]) {
                acc[curr.bowler] += parseInt(curr.total_runs)
            }
            else {
                acc[curr.bowler] = parseInt(curr.total_runs)
            }
        }
        return acc
    }, {})

    let bowlers = Object.keys(ballsBowledInSuperOvers)
    let economyOfBowlers = Object.entries(bowlers.reduce((acc, curr) => {
        let economy = Math.round(runsConcededInSuperOvers[curr] / (ballsBowledInSuperOvers[curr] / 6))
        acc[curr] = economy
        return acc
    }, {})).sort((firstValue, secondValue) => {
        if (firstValue[1] > secondValue[1]) return 1
        else if (firstValue[1] < secondValue[1]) return -1
        else return 0
    })
    return economyOfBowlers
}


const result = getEconomyOfBowlers(deliveriesJson)
console.log(result)

const superOversBestEconomyBowlerJSON = JSON.stringify(result)

fs.writeFile("../public/output/superOversBestEconomyBowler.json", superOversBestEconomyBowlerJSON, (err) => {
  if (err) {
    console.error(err)
    return
  }
  //file written successfully
})