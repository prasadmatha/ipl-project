//Find a player who has won the highest number of Player of the Match awards for each season

const fs = require('fs')
const { matchesJson, deliveriesJson } = require("./index.js")

function getPlayersWonHighestAwardsEachSeason(matchesJson) {
    let playersWonHighestAwardsEachSeason = Object.fromEntries(Object.entries(matchesJson.reduce((acc, curr) => {
        if (acc[curr.season]) {
            if (acc[curr.season][curr.player_of_match]) {
                acc[curr.season][curr.player_of_match] += 1
            }
            else {
                acc[curr.season][curr.player_of_match] = 1
            }
        }
        else {
            acc[curr.season] = {}
            acc[curr.season][curr.player_of_match] = 1
        }
        return acc
    }, {})).map(each => {
        let maxValue = Math.max(...Object.values(each[1]))
        let array = Object.entries(each[1]).filter(each => {
            return each[1] === maxValue
        })
        return [each[0], Object.fromEntries(array)]
    }))
    return playersWonHighestAwardsEachSeason
}

const result = getPlayersWonHighestAwardsEachSeason(matchesJson)
console.log(result)

const playersWonHighestAwardsEachSeasonJSON = JSON.stringify(result)

fs.writeFile("../public/output/playersWonHighestAwardsEachSeason.json", playersWonHighestAwardsEachSeasonJSON, (err) => {
  if (err) {
    console.error(err)
    return
  }
  //file written successfully
})