let csvToJson = require('convert-csv-to-json');

let matchescsvFilePath = "../data/matches.csv"
let deliveriescsvFilePath = "../data/deliveries.csv"
let matchesJson = csvToJson.fieldDelimiter(',').getJsonFromCsv(matchescsvFilePath)
let deliveriesJson = csvToJson.fieldDelimiter(',').getJsonFromCsv(deliveriescsvFilePath)

exports.matchesJson = matchesJson
exports.deliveriesJson = deliveriesJson