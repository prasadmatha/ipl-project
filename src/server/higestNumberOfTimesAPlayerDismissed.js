//Find the highest number of times one player has been dismissed by another player

const fs = require('fs')
const { matchesJson, deliveriesJson } = require("./index.js")

function getPlayersWithNumberOfDismissals(deliveriesJson){
    let playersWithNumberOfDismissals = Object.fromEntries(Object.entries(deliveriesJson.reduce((acc, curr) => {
        if (curr.player_dismissed != "" && curr.dismissal_kind !== "run out" && curr.dismissal_kind !== "retired hurt") {
            let player = curr.player_dismissed
            let bowler = curr.bowler
            if (acc[player]) {
                if (acc[player][bowler]) {
                    acc[player][bowler] += 1
                }
                else {
                    acc[player][bowler] = 1
                }
            }
            else {
                acc[player] = {}
                acc[player][bowler] = 1
            }
        }
        return acc
    }, {})).map(each => {
        let player = each[0]
        let maxValue = Math.max(...(Object.values(each[1])))
        let dismissals = Object.fromEntries(Object.entries(each[1]).filter(each => {
            return each[1] === maxValue
        }))
        return [player, dismissals]
    }))
    return playersWithNumberOfDismissals
}


const result = getPlayersWithNumberOfDismissals(deliveriesJson)
console.log(result)

const higestNumberOfTimesAPlayerDismissedJSON = JSON.stringify(result)

fs.writeFile("../public/output/higestNumberOfTimesAPlayerDismissed.json", higestNumberOfTimesAPlayerDismissedJSON, (err) => {
  if (err) {
    console.error(err)
    return
  }
  //file written successfully
})