//Find the strike rate of a batsman for each season

const fs = require('fs')
const { matchesJson, deliveriesJson } = require("./index.js")


function getStrikeRateOfBatsmanEachSeason(matchesJson, deliveriesJson) {
    let years = matchesJson.reduce((acc, curr) => {
        if (!acc.includes(curr.season)) {
            acc.push(curr.season)
        }
        return acc
    }, [])
    let strikeRateOfBatsmanEachSeason = Object.entries(deliveriesJson.reduce((acc, curr) => {
        let array = matchesJson.filter(each => {
            return (each.id === curr.match_id)
        })
        let season = array[0].season
        if (acc[curr.batsman]) {
            if (acc[curr.batsman][season]) {
                acc[curr.batsman][season]["runsScored"] += parseInt(curr.batsman_runs)
                acc[curr.batsman][season]["ballsFaced"] += 1
            }
            else {
                acc[curr.batsman][season] = {}
                acc[curr.batsman][season]["runsScored"] = parseInt(curr.batsman_runs)
                acc[curr.batsman][season]["ballsFaced"] = 1
            }
        } else {
            acc[curr.batsman] = {}
            acc[curr.batsman][season] = {}
            acc[curr.batsman][season]["runsScored"] = parseInt(curr.batsman_runs)
            acc[curr.batsman][season]["ballsFaced"] = 1
        }
        return acc
    }, {})).reduce((acc, curr) => {
        let player = curr[0]
        let calculateStrikeRate = years.reduce((acc1, year) => {
            if (curr[1][year]) {
                acc1[year] = Number(((curr[1][year]["runsScored"] / curr[1][year]["ballsFaced"]) * 100).toFixed(2))
            }
            return acc1
        }, {})
        acc[player] = calculateStrikeRate
        return acc
    }, {})
    return strikeRateOfBatsmanEachSeason
}

const result = getStrikeRateOfBatsmanEachSeason(matchesJson, deliveriesJson)
console.log(result)

const strikeRateOfBatsmanEachSeasonJSON = JSON.stringify(result)

fs.writeFile("../public/output/strikeRateOfBatsmanEachSeason.json", strikeRateOfBatsmanEachSeasonJSON, (err) => {
  if (err) {
    console.error(err)
    return
  }
  //file written successfully
})