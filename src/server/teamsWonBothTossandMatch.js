//Find the number of times each team won the toss and also won the match

const fs = require('fs')
const { matchesJson, deliveriesJson } = require("./index.js")


function getTeamsWonBothTossandMatch(matchesJson){
    const teamsWonBothTossandMatch = matchesJson.reduce((acc, curr) => {
        if (curr.toss_winner === curr.winner) {
            (acc[curr.winner]) ? acc[curr.winner] += 1 : acc[curr.winner] = 1
        }
        return acc
    },{})
    return teamsWonBothTossandMatch
}

const result = getTeamsWonBothTossandMatch(matchesJson)
console.log(result)

const teamsWonBothTossandMatchJSON = JSON.stringify(result)

fs.writeFile("../public/output/teamsWonBothTossandMatch.json", teamsWonBothTossandMatchJSON, (err) => {
  if (err) {
    console.error(err)
    return
  }
  //file written successfully
})