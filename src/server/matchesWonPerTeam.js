//2. Number of matches won per team per year in IPL.

const fs = require('fs')
const { matchesJson, deliveriesJson } = require("./index.js")

function getMatchesWonPerTeam(matchesJson) {
    let matchesWonPerTeamPerYear = matchesJson.reduce((acc, curr) => {
        let team = curr.winner
        let year = curr.season
        if (curr.result === "normal") {
            if (acc[team]) {
                if (acc[team][year]) {
                    acc[team][year] += 1
                }
                else {
                    acc[team][year] = 1
                }
            }
            else {
                acc[team] = {}
                acc[team][year] = 1
            }
        }
        return acc
    }, {})
    return matchesWonPerTeamPerYear
}

const result = getMatchesWonPerTeam(matchesJson)
console.log(result)

const matchesWonPerTeamJSON = JSON.stringify(result)

fs.writeFile("../public/output/matchesWonPerTeam.json", matchesWonPerTeamJSON, (err) => {
  if (err) {
    console.error(err)
    return
  }
  //file written successfully
})
